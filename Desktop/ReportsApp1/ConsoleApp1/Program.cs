﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Net;
using System.Net.Http;
using System.Diagnostics;
using System.IO;
using System.Globalization;
using System.Drawing;
using System.Text.RegularExpressions;

namespace ConsoleApp1
{

    public class Program
    {

        static void Main(string[] args)
        {
            Stopwatch all = new Stopwatch();

            string path = @"C:\Users\Alienware\Desktop\bjorn\ReportsApp1";
            ReportModel reportModel = new ReportModel
            {
                Id = "6",
                Title = "Unexpected result in the software service",
                CategoryTitle = "Unexpected result in the software service",
                StartDate = DateTime.Now.AddDays(-2),
                CreatedDateTime = DateTime.Now.AddDays(2),
                CreatedByName = "Vidar Aadnevik",
                DimensionName = "dimension name",
                Handlers = new List<string> { "Inger Anette Lorentzen", "Vidar Aadnevik", "Bjorn Nese" },
                Status = ReportStatus.CorrectiveActionStarted,
                Descriptions = new List<ReportDescription> { new ReportDescription { Type = DescriptionType.General, Description = "DescriptionString" }, new ReportDescription { Type = DescriptionType.Reason, Description = "DescriptionString2" } },
                Files = new List<ReportFile> {
                    new ReportFile { ContentType = ".jpg", Filename = "bla.jpg", Bytes = ImageToBase64(Image.FromFile(path + @"\\ReportsApp\Resources\logo_oa.png"),  System.Drawing.Imaging.ImageFormat.Png ) },
                    new ReportFile { ContentType = ".jpg", Filename = "bla.jpg", Bytes = ImageToBase64(Image.FromFile(path + @"\ReportsApp\Resources\logo_oa.png"),  System.Drawing.Imaging.ImageFormat.Png ) },
                },
                ConfigColumns = "1",
                comments = new List<ReportComment> { new ReportComment { CreatedBy = "CreatedBy String", CommentText = "CommentText String", DateTimeCreated = DateTime.Today, Guid = Guid.NewGuid(), NumericReportId = 5, ReportId = "3", ReportStatus = 6, SystemId = "SystemId string" } },
                entries = new List<entry> { new entry { DateTime = DateTime.Now.AddDays(5) } },
                CorrectiveActions = new List<CorrectiveAction> { new CorrectiveAction
                                                                {
                                                                  AssignedTo =  new List<string> { "Assined1", "Assined2" , "Assined2"  },
                                                                  CreateDateTime = DateTime.Now.AddDays(2),
                                                                  Description = "Description String",
                                                                  ExpectedImplementationDate =  DateTime.Now.AddMonths(2),
                                                                  ImplementationDegree = 5,
                                                                  PrimaryAction = true,
                                                                    ReportId = "3",
                                                                ResultDate =  DateTime.Now.AddMonths(2) ,
                                                                     SystemId = "77",
                                                                   ImplementationUser = "ImplementationUser String",
                                                                ResultComment = "ResultComment string "}
                                                                },
                SystemId = "78",
                NumericDimensionId = 34,
                NumericCategoryId = 35,
                DimensionId = "36",
                CategoryId = "37",
                CategoryCode = "CategoryCode string",
                SecurityLevel = "SecurityLevel string ",
                CreatedByUserId = "35",
                DimensionUniqueCode = "678",
                ImpactDescription = "ImpactDescription string",
                PotensialImpactQuantityUnit = "PotensialImpactQuantityUnit",
                PotensialImpactQuantityPower = 23432432432,
                EndDate = DateTime.Now.AddDays(4),
                ParentDimensionSequence = new List<ParentDimension> { new ParentDimension { DimensionId = "36", NumericId = 45 }, new ParentDimension { DimensionId = "36", NumericId = 45 } },
                UserAccesses = new List<ReportAccess> { new ReportAccess { AccessLevel = 34, CanDelete  = true , CanEdit = true , IsHandler = false, IsObserver = true, Locked = false, UserId = "24", Notification = 9 } , new ReportAccess { AccessLevel = 34, CanDelete = true, CanEdit = true, IsHandler = false, IsObserver = true, Locked = false, UserId = "25", Notification = 10} },
                Units = new List<ReportUnit> { new ReportUnit { Name = "Report unit name", Power = 324234, Unit = "Unit string " } , new ReportUnit { Name = "Report unit name2", Power = 12313, Unit = "Unit string1 " } },
                Tags = new List<string> { "tag1","tag2","tag3" },
                NumericReportId = 14,
                ParentDimensionSequenceString = "ParentDimensionSequenceString",
                NonConformityLevel = "NonConformityLevel",
                CloneId = "GeoData",
                GeoData = "GeoData",
                ReadTimeOut = true,
                HandlingTimeOut = false,
                LastExpiryProcessDate = DateTime.Today,
                HandlingExpiryNotificationDate = DateTime.Today.AddYears(2),
                IsPartOfTendency = true ,
                directUrl = @"http://localhost:49911/API/EXCEL",
    };
            ReportModel reportModel1 = new ReportModel
            {
                Id = "2",
                Title = "samplestring22",               
                CategoryTitle = "Uønsket resultat i programvaretjenesten",
                StartDate = DateTime.Now.AddDays(-2),
                CreatedDateTime = DateTime.Now.AddDays(2),
                CreatedByName = "Vidar Aadnevik",
                DimensionName = "dimensjon navn",
                Handlers = new List<string> { "Inger Anette Lorentzen", "Vidar Aadnevik", "Bjorn Nese" },
                Status = ReportStatus.CorrectiveActionStarted,
                Descriptions = new List<ReportDescription> { new ReportDescription { Type = DescriptionType.General, Description = "DescriptionString" } },
                Files = new List<ReportFile> {
                   new ReportFile { ContentType = ".jpg", Filename = "bla.jpg", Bytes = ImageToBase64(Image.FromFile(path + @"\\ReportsApp\Resources\testpng.png"),  System.Drawing.Imaging.ImageFormat.Png ) },
                },
                ConfigColumns = "6",
                comments = new List<ReportComment> { new ReportComment { CreatedBy = "CreatedBy String", CommentText = "CommentText String", DateTimeCreated = DateTime.Today, Guid = Guid.NewGuid(), NumericReportId = 5, ReportId = "3", ReportStatus = 6, SystemId = "SystemId string" } },
                directUrl = @"http://localhost:49911/API/EXCEL",
            };

            List<ReportModel> reports = new List<ReportModel>();
            reports.Add(reportModel);
            reports.Add(reportModel);
          //  reports.Add(reportModel1);
          
         //  reports.Add(reportModel1);

            ReportListModel lista = new ReportListModel()
            {
                Reports = reports,
                LogoBase64String = Convert.ToBase64String(System.IO.File.ReadAllBytes(path + @"\\ReportsApp\Resources\logo_oa.png")),
                ImageDpi = 72,
                Culture = "en-US",
                ReportTitle = "ReportTitleString",
                ApiKey = "12345!",
                ApiPassword = "12345!",
            };

            var jsonReportFile = JsonConvert.SerializeObject(reportModel);

            Console.WriteLine("Making API Call...");
            all.Start();
            using (var client = new HttpClient(new HttpClientHandler { AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate }))
            {
                var stringContent = new StringContent(JsonConvert.SerializeObject(lista), Encoding.UTF8, "application/json");
                var response = client.PostAsync("http://localhost:49911/API/EXCEL", stringContent);
             //   var response = client.PostAsync("http://localhost:49911/API/PDF", stringContent);
               // var response = client.PostAsync("http://78.46.218.2:80/API/PDF", stringContent);
                all.Stop();
                Console.WriteLine("Result: " + response.Result.Content.ReadAsStringAsync().Result);

                var rsponseFile = new Rsponse();
                rsponseFile = JsonConvert.DeserializeObject<Rsponse>(response.Result.Content.ReadAsStringAsync().Result);

                byte[] sPDFDecoded = @Convert.FromBase64String(rsponseFile.message);
                System.IO.FileStream stream = new FileStream(path + @"\pdf.xls", FileMode.Create);
                System.IO.BinaryWriter writer1 = new BinaryWriter(stream);
                writer1.Write(sPDFDecoded, 0, sPDFDecoded.Length);
                writer1.Close();
            }
            Console.WriteLine(all.Elapsed.TotalSeconds);
            Console.ReadLine();
        }
        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }

        public static string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }

        public static string ImageToBase64(System.Drawing.Image image, System.Drawing.Imaging.ImageFormat format)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                // Convert Image to byte[]
                image.Save(ms, format);
                byte[] imageBytes = ms.ToArray();
                // Convert byte[] to Base64 String
                string base64String =  Convert.ToBase64String(imageBytes) ;
                return base64String; 
            }
        }


        public static System.Drawing.Image Base64ToImage(string base64String)
        {
            // Convert Base64 String to byte[]
            byte[] imageBytes = Convert.FromBase64String(base64String);

            byte[] imageBytesDecoded = Encoding.UTF8.GetBytes(imageBytes.ToString());
            MemoryStream ms = new MemoryStream(imageBytes, 0, imageBytes.Length);
            // Convert byte[] to Image
            ms.Write(imageBytes, 0, imageBytes.Length);
            System.Drawing.Image image = System.Drawing.Image.FromStream(ms, true);
            return image;
        }

      

    }
}
