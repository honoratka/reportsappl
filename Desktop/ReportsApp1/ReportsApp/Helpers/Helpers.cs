﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using iTextSharp;
using iTextSharp.text;
using System.IO;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using System.Threading.Tasks;
//using System.Web.Mvc;
using ReportsApp.Models;
using System.Web.Http;
using System.Diagnostics;
using Newtonsoft.Json;
using System.Web.Mvc;
using System.Globalization;
using System.Threading;
using System.Resources;
using System.Text;

namespace ReportsApp.Helpers
{
    public class Helpers
    {
        //convert memorystream to byte array
        public static byte[] ReadFully(Stream input)
        {
            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }

        public static string ImageToBase64(System.Drawing.Image image, System.Drawing.Imaging.ImageFormat format)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                // Convert Image to byte[]
                image.Save(ms, format);
                byte[] imageBytes = ms.ToArray();
                //
                byte[] data = Encoding.UTF8.GetBytes(imageBytes.ToString());

                // Convert byte[] to Base64 String
                string base64String = Convert.ToBase64String(data);
                return base64String;
            }
        }


        public static System.Drawing.Image Base64ToImage(string base64String)
        {
            // Convert Base64 String to byte[]
            byte[] imageBytes = Convert.FromBase64String(base64String);

            byte[] imageBytesDecoded = Encoding.UTF8.GetBytes(imageBytes.ToString());
            MemoryStream ms = new MemoryStream(imageBytes, 0, imageBytes.Length);
            // Convert byte[] to Image
            ms.Write(imageBytes, 0, imageBytes.Length);
            System.Drawing.Image image = System.Drawing.Image.FromStream(ms, true);
            return image;
        }


        //convert Image to  byte[]
        public static byte[] imageToByteArray(System.Drawing.Image imageIn)
        {
            MemoryStream ms = new MemoryStream();
            imageIn.Save(ms, System.Drawing.Imaging.ImageFormat.Gif);
            return ms.ToArray();
        }

        // convert date to ToLongDateString with specyfic CultureInfo
        public static string DateLanguageConversion(DateTime date, CultureInfo cultureInfo)
        {            
            Thread.CurrentThread.CurrentCulture = cultureInfo;
            return date.ToLongDateString();
        }

        // Get string from resorces
        public static string StringResXLanguageConversion(ResourceManager rm, string str, CultureInfo cultureInfo)
        {      
            Thread.CurrentThread.CurrentUICulture = cultureInfo;

            string ret = rm.GetString(str);

            return ret;
        }

        /// Excel Null chceker 
       public static ExcelColumnEmptyNumber NullChceker  ( int columnnumber , string stringtocheck ) {

            bool isEmpty = false;


            if (stringtocheck == "" || stringtocheck == "//")
            {
                isEmpty = true;
            }


            return new ExcelColumnEmptyNumber() {  ColumnNumber = columnnumber , IsColumnEmpty = isEmpty };
        }
    }
}