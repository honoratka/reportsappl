﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.IO;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using BottomBorder = DocumentFormat.OpenXml.Spreadsheet.BottomBorder;
using Fill = DocumentFormat.OpenXml.Spreadsheet.Fill;
using Fonts = DocumentFormat.OpenXml.Spreadsheet.Fonts;
using ForegroundColor = DocumentFormat.OpenXml.Spreadsheet.ForegroundColor;
using Hyperlink = DocumentFormat.OpenXml.Spreadsheet.Hyperlink;
using LeftBorder = DocumentFormat.OpenXml.Spreadsheet.LeftBorder;
using NumberingFormat = DocumentFormat.OpenXml.Spreadsheet.NumberingFormat;
using PatternFill = DocumentFormat.OpenXml.Spreadsheet.PatternFill;
using RightBorder = DocumentFormat.OpenXml.Spreadsheet.RightBorder;
using Text = DocumentFormat.OpenXml.Spreadsheet.Text;
using TopBorder = DocumentFormat.OpenXml.Spreadsheet.TopBorder;

namespace ReportsApp.Reporting
{
    public class SpreadSheetService 
    {
        public MemoryStream SpreadsheetStream { get; set; } // The stream that the spreadsheet gets returned on
        private static Worksheet CurrentWorkSheet { get { return _spreadSheet.WorkbookPart.WorksheetParts.First().Worksheet; } }
        private static SpreadsheetDocument _spreadSheet;
        private Columns _cols;
 

        public bool CreateSpreadsheet()
        {
            try
            {
                SpreadsheetStream = new MemoryStream();
 
                // Create the spreadsheet on the MemoryStream
                _spreadSheet = SpreadsheetDocument.Create(SpreadsheetStream, SpreadsheetDocumentType.Workbook);
 
                var wbp = _spreadSheet.AddWorkbookPart();   // Add workbook part
                //WorksheetPart wsp = wbp.AddNewPart<WorksheetPart>(); // Add worksheet part
                var wb = new Workbook(); // Workbook
                var fv = new FileVersion();
                fv.ApplicationName = "Unified Reporting";
                var ws = new Worksheet(); // Worksheet
                var sd = new SheetData(); // Data on worksheet
 
                // Add stylesheet
                var stylesPart = _spreadSheet.WorkbookPart.AddNewPart<WorkbookStylesPart>();
                stylesPart.Stylesheet = GenerateStyleSheet();
                stylesPart.Stylesheet.Save();
 
                
                ws.Append(sd); // Add sheet data to worksheet
                
                
                 
                
                _spreadSheet.WorkbookPart.Workbook = wb;
                _spreadSheet.WorkbookPart.Workbook.Save();
                
            }
            catch
            {
                return false;
            }
 
            return true;
        }
 
 
        public string AddSheet(string name)
        {
            var wsp = _spreadSheet.WorkbookPart.AddNewPart<WorksheetPart>();
            wsp.Worksheet = new Worksheet();
 
            wsp.Worksheet.AppendChild(new DocumentFormat.OpenXml.Spreadsheet.SheetData());
 
            wsp.Worksheet.Save();
 
            UInt32 sheetId;
 
            // If this is the first sheet, the ID will be 1. If this is not the first sheet, we calculate the ID based on the number of existing
            // sheets + 1.
            if (_spreadSheet.WorkbookPart.Workbook.Sheets == null)
            {
                _spreadSheet.WorkbookPart.Workbook.AppendChild(new Sheets());
                sheetId = 1;
            }
            else
            {
                sheetId = Convert.ToUInt32(_spreadSheet.WorkbookPart.Workbook.Sheets.Count() + 1);
            }
 
            // Create the new sheet and add it to the workbookpart
            _spreadSheet.WorkbookPart.Workbook.GetFirstChild<Sheets>().AppendChild(new Sheet
            {
                Id = _spreadSheet.WorkbookPart.GetIdOfPart(wsp),
                SheetId = sheetId,
                Name = name
            }
            );
 
            _cols = new Columns(); // Created to allow bespoke width columns
            // Save our changes
            _spreadSheet.WorkbookPart.Workbook.Save();
 
            return _spreadSheet.WorkbookPart.GetIdOfPart(wsp);// wsp;
        }
 
        private Worksheet _getWorkSheet(string sheetId)
        {
            var wsp = (WorksheetPart)_spreadSheet.WorkbookPart.GetPartById(sheetId);
            return wsp.Worksheet;
        }
 
        /// <summary>
        /// add the bespoke columns for the list spreadsheet
        /// </summary>
        public void CreateColumnWidth(string sheetId, uint startIndex, uint endIndex, double width)
        {
            // Find the columns in the worksheet and remove them all
            
            if (_getWorkSheet(sheetId).Where(x => x.LocalName == "cols").Count() > 0)
                _getWorkSheet(sheetId).RemoveChild(_cols);
 
            // Create the column
            var column = new Column();
            column.Min = startIndex;
            column.Max = endIndex;
            column.Width = width;
            column.CustomWidth = true;
            _cols.Append(column); // Add it to the list of columns
 
            // Make sure that the column info is inserted *before* the sheetdata
            
            _getWorkSheet(sheetId).InsertBefore<Columns>(_cols, _getWorkSheet(sheetId).Where(x => x.LocalName == "sheetData").First());
            _getWorkSheet(sheetId).Save();
            _spreadSheet.WorkbookPart.Workbook.Save();

            var wb = _spreadSheet.WorkbookPart;
            var wsp = wb.WorksheetParts.First();

        }



        /// <summary>
        /// Close the spreadsheet
        /// </summary>
        public void CloseSpreadsheet()
        {
            _spreadSheet.Close();
        }
 
        /// <summary>
        /// Pass a list of column headings to create the header row
        /// </summary>
        /// <param name="headers"></param>
        public void AddHeader(string sheetId, List<string> headers)
        {
            // Find the sheetdata of the worksheet
            
            SheetData sd = (SheetData)_getWorkSheet(sheetId).Where(x => x.LocalName == "sheetData").First();
            Row header = new Row();
            // increment the row index to the next row
            header.RowIndex = Convert.ToUInt32(sd.ChildElements.Count()) + 1;
            sd.Append(header); // Add the header row
 
            foreach (string heading in headers)
            {
                AppendCell(sheetId, header, header.RowIndex, heading, 1, "");
            }
 
            // save worksheet
             
            _getWorkSheet(sheetId).Save();
            
        }
        private static bool AddSharedString(SpreadsheetDocument spreadsheet, string stringItem, bool save)
        {
            var sharedStringTable = spreadsheet.WorkbookPart.SharedStringTablePart.SharedStringTable;

            if (0 == sharedStringTable.Where(item => item.InnerText == stringItem).Count())
            {
                sharedStringTable.AppendChild(new SharedStringItem(new Text(stringItem)));

                if (save)
                {
                    sharedStringTable.Save();
                }
            }

            return true;
        }
        public static bool SetCellValue(SpreadsheetDocument spreadsheet, Worksheet worksheet, uint columnIndex, uint rowIndex, string stringValue, bool useSharedString, uint? styleIndex, bool save)
        {
            string columnValue = stringValue;
            CellValues cellValueType;

            if (useSharedString)
            {
                if (IndexOfSharedString(spreadsheet, stringValue) == -1)
                {
                    AddSharedString(spreadsheet, stringValue, true);
                }

                columnValue = IndexOfSharedString(spreadsheet, stringValue).ToString();
                cellValueType = CellValues.SharedString;
            }
            else
            {
                cellValueType = CellValues.String;
            }

            return SetCellValue(worksheet, columnIndex, rowIndex, cellValueType, columnValue, styleIndex, save);
        }
        private static bool SetCellValue(Worksheet worksheet, uint columnIndex, uint rowIndex, CellValues valueType, string value, uint? styleIndex, bool save)
        {
            var sheetData = worksheet.GetFirstChild<SheetData>();
            Row row;
            Row previousRow = null;
            Cell cell;
            Cell previousCell = null;
            Columns columns;
            Column previousColumn = null;
            string cellAddress = ColumnNameFromIndex(columnIndex) + rowIndex;

            if (sheetData.Elements<Row>().Where(item => item.RowIndex == rowIndex).Count() != 0)
            {
                row = sheetData.Elements<Row>().Where(item => item.RowIndex == rowIndex).First();
            }
            else
            {
                row = new Row { RowIndex = rowIndex };

                for (uint counter = rowIndex - 1; counter > 0; counter--)
                {
                    previousRow = sheetData.Elements<Row>().Where(item => item.RowIndex == counter).FirstOrDefault();
                    if (previousRow != null)
                    {
                        break;
                    }
                }

                sheetData.InsertAfter(row, previousRow);
            }

            if (row.Elements<Cell>().Where(item => item.CellReference.Value == cellAddress).Count() > 0)
            {
                cell = row.Elements<Cell>().Where(item => item.CellReference.Value == cellAddress).First();
            }
            else
            {
                for (uint counter = columnIndex - 1; counter > 0; counter--)
                {
                    previousCell = row.Elements<Cell>().Where(item => item.CellReference.Value == ColumnNameFromIndex(counter) + rowIndex).FirstOrDefault();
                    if (previousCell != null)
                    {
                        break;
                    }
                }

                cell = new Cell { CellReference = cellAddress, StyleIndex = styleIndex };

                row.InsertAfter(cell, previousCell);
            }

            columns = worksheet.Elements<Columns>().FirstOrDefault();
            if (columns == null)
            {
                columns = worksheet.InsertAt(new Columns(), 0);
            }

            if (columns.Elements<Column>().Where(item => item.Min == columnIndex).Count() == 0)
            {
                for (uint counter = columnIndex - 1; counter > 0; counter--)
                {
                    previousColumn = columns.Elements<Column>().Where(item => item.Min == counter).FirstOrDefault();
                    if (previousColumn != null)
                    {
                        break;
                    }
                }

                columns.InsertAfter(new Column
                {
                    Min = columnIndex,
                    Max = columnIndex,
                    CustomWidth = true,
                    Width = 9
                }, previousColumn);
            }

            cell.CellValue = new CellValue(value);
            if (styleIndex != null)
            {
                cell.StyleIndex = styleIndex;
            }

            if (valueType != CellValues.Date)
            {
                cell.DataType = new EnumValue<CellValues>(valueType);
            }

            if (save)
            {
                worksheet.Save();
            }

            return true;
        }

        private static string ColumnNameFromIndex(uint columnIndex)
        {
            string columnName = "";

            while (columnIndex > 0)
            {
                uint remainder = (columnIndex - 1) % 26;
                columnName = Convert.ToChar(65 + remainder).ToString() + columnName;
                columnIndex = (uint)((columnIndex - remainder) / 26);
            }

            return columnName;
        }
       
        private static int IndexOfSharedString(SpreadsheetDocument spreadsheet, string stringItem)
        {

            if (spreadsheet.WorkbookPart.SharedStringTablePart == null)
            {
                return -1;
            }

            SharedStringTable sharedStringTable = spreadsheet.WorkbookPart.SharedStringTablePart.SharedStringTable;
            bool found = false;
            int index = 0;

            foreach (SharedStringItem sharedString in sharedStringTable.Elements<SharedStringItem>())
            {
                if (sharedString.InnerText == stringItem)
                {
                    found = true;
                    break;
                }
                index++;
            }

            if (found)
            {
                return index;
            }

            return -1;
        }

       

        private static HyperlinkRelationship SetHyperlinkRelationship(Worksheet worksheet, string link)
        {
            return worksheet.WorksheetPart.AddHyperlinkRelationship(new Uri(link, UriKind.Absolute), true);
        }

        public static bool SetCellHyperlink(string sheetname,Row row, uint columnIndex, uint rowIndex, string linkValue, bool save)
        {
            var wsp = (WorksheetPart)_spreadSheet.WorkbookPart.GetPartById(sheetname);
            var curr = wsp.Worksheet;

            var hyperlinkRelationship = SetHyperlinkRelationship(curr, linkValue);
            
            
            Hyperlinks hyperlinks;
            if (!curr.Elements<Hyperlinks>().Any())
            {
                hyperlinks = new Hyperlinks();
                curr.InsertAfter(hyperlinks, curr.Descendants<SheetData>().FirstOrDefault<SheetData>());
            }
            else
            {
                hyperlinks = curr.Elements<Hyperlinks>().First<Hyperlinks>();
            }

            Hyperlink hyperlink = new Hyperlink { Reference = ColumnNameFromIndex(columnIndex) + rowIndex, Id = hyperlinkRelationship.Id };
            
            hyperlinks.Append(hyperlink);

//            if (save)
//            {
//                CurrentWorkSheet.Save();
//            }

            return true;
        }

        public void SaveChanges(string sheetId)
        {
            _getWorkSheet(sheetId).Save();
        }

        public void AddRow(string sheetId, List<string> dataItems, string url)
        {
            // Find the sheetdata of the worksheet
             
            SheetData sd = (SheetData)_getWorkSheet(sheetId).First(x => x.LocalName == "sheetData");
            Row header = new Row();
            // increment the row index to the next row
            header.RowIndex = Convert.ToUInt32(sd.ChildElements.Count()) + 1;

            sd.Append(header);
 
            foreach (var item in dataItems)
            {
                if (IsDate(item))
                {
                    DateTime dt;
                    DateTime.TryParse(item, out dt);
                    AppendCell(sheetId, header, header.RowIndex, dt, 0, url);
                }
                else
                {
                    AppendCell(sheetId, header, header.RowIndex, item, 0, url);
                }
                
            }

            // save worksheet
           
        }
        protected bool IsNumeric(string value)
        {
            double isNum;
            var res = double.TryParse(value, out isNum);
            return res;
        }

        protected bool IsDate(string date)
        {
            DateTime isDate;
            var res =DateTime.TryParse(date,out isDate);
            return res;
        }

        private void AppendCell(string sheetname, Row row, uint rowIndex, DateTime value, uint styleIndex, string url)
        {
            var cell = new Cell();
            cell.StyleIndex = 7;  // Style index comes from stylesheet generated in GenerateStyleSheet()
            //cell.DataType = CellValues.Date;
            cell.CellValue = new CellValue(value.ToOADate().ToString(CultureInfo.InvariantCulture));
            

            // Get the last cell's column
            var nextCol = "A";
            var c = (Cell)row.LastChild;

            if (c != null) // if there are some cells already there...
            {
                var numIndex = c.CellReference.ToString().IndexOfAny(new char[] { '1', '2', '3', '4', '5', '6', '7', '8', '9' });
                // Get the last column reference
                var lastCol = c.CellReference.ToString().Substring(0, numIndex);
                // Increment
                nextCol = IncrementColRef(lastCol);
            }

            cell.CellReference = nextCol + rowIndex;

            if (!string.IsNullOrEmpty(url) && nextCol == "A")
            {
                SetCellHyperlink(sheetname, row, 1, rowIndex, url, true);
            }
            row.AppendChild(cell);
        }


        /// <summary>
        /// Add cell into the passed row.
        /// </summary>
        /// <param name="row"></param>
        /// <param name="rowIndex"></param>
        /// <param name="value"></param>
        /// <param name="styleIndex"></param>
        private void AppendCell(string sheetname, Row row,  uint rowIndex, string value, uint styleIndex, string url)
        {
            var cell = new Cell();
            cell.StyleIndex = styleIndex;  // Style index comes from stylesheet generated in GenerateStyleSheet()

            if (IsNumeric(value))
            {
                cell.DataType = CellValues.Number;
                cell.CellValue = new CellValue(value);
            }
            else
            {
                if (!string.IsNullOrEmpty(value))
                {
                    cell.DataType = CellValues.InlineString;
                    var t = new Text();
                    t.Text = value;
                    // Append Text to InlineString object
                    var inlineString = new InlineString();
                    inlineString.AppendChild(t);

                    // Append InlineString to Cell
                    cell.AppendChild(inlineString);

                }
                else
                {
                    //cell.DataType = CellValues.Number;
                    cell.CellValue = new CellValue(null);
                }
            }

            // Get the last cell's column
            var nextCol = "A";
            var c = (Cell)row.LastChild;

            if (c != null) // if there are some cells already there...
            {
                int numIndex = c.CellReference.ToString().IndexOfAny(new char[] { '1', '2', '3', '4', '5', '6', '7', '8', '9' });
 
                // Get the last column reference
                string lastCol = c.CellReference.ToString().Substring(0, numIndex);
                // Increment
                nextCol = IncrementColRef(lastCol);
            }

            cell.CellReference = nextCol + rowIndex;

            if (!String.IsNullOrEmpty(url) && nextCol =="A")
            {

                SetCellHyperlink(sheetname, row,1, rowIndex, url, true);
            }            
            row.AppendChild(cell);
        }
 
        // Increment the column reference in an Excel fashion, i.e. A, B, C...Z, AA, AB etc.
        // Partly stolen from somewhere on the Net and modified for my use.
        private string IncrementColRef(string lastRef)
        {
            char[] characters = lastRef.ToUpperInvariant().ToCharArray();
            int sum = 0;
            for (int i = 0; i < characters.Length; i++)
            {
                sum *= 26;
                sum += (characters[i] - 'A' + 1);
            }
 
            sum++;
 
            string columnName = String.Empty;
            int modulo;
 
            while (sum > 0)
            {
                modulo = (sum - 1) % 26;
                columnName = Convert.ToChar(65 + modulo).ToString() + columnName;
                sum = (int)((sum - modulo) / 26);
            }
 
            return columnName;
 
 
        }

        /// <summary>
        /// Return a stylesheet. Completely stolen from somewhere, possibly this guy's blog,
        /// although I can't find it on there:
        /// http://polymathprogrammer.com/. Thanks whoever it was, it would have been a
        /// nightmare trying to figure this one out!
        /// </summary>
        /// <returns></returns>
        private Stylesheet GenerateStyleSheet()
        {
            
            return new Stylesheet(
                new NumberingFormats(
                    new NumberingFormat{
                         NumberFormatId = UInt32Value.FromUInt32(165),
                         FormatCode = StringValue.FromString("dd.MM.yy hh:mm")
                        }
                    ),
                new Fonts(
                    new Font( // Index 0 - The default font.
                        new FontSize {Val = 11},
                        new Color {Rgb = new HexBinaryValue() {Value = "000000"}},
                        new FontName {Val = "Calibri"}),
                    new Font( // Index 1 - The bold font.
                        new Bold(),
                        new FontSize {Val = 11},
                        new Color {Rgb = new HexBinaryValue() {Value = "000000"}},
                        new FontName {Val = "Calibri"}),
                    new Font( // Index 2 - The Italic font.
                        new Italic(),
                        new FontSize {Val = 11},
                        new Color {Rgb = new HexBinaryValue() {Value = "000000"}},
                        new FontName {Val = "Calibri"}),
                    new Font( // Index 2 - The Times Roman font. with 16 size
                        new FontSize {Val = 16},
                        new Color {Rgb = new HexBinaryValue() {Value = "000000"}},
                        new FontName {Val = "Times New Roman"})
                    ),
                new Fills(
                    new Fill( // Index 0 - The default fill.
                        new PatternFill {PatternType = PatternValues.None}),
                    new Fill( // Index 1 - The default fill of gray 125 (Required)
                        new PatternFill {PatternType = PatternValues.Gray125}),
                    new Fill( // Index 2 - The yellow fill.
                        new PatternFill(
                            new ForegroundColor {Rgb = new HexBinaryValue() {Value = "FFFFFF00"}}
                            ) {PatternType = PatternValues.Solid})
                    ),
                new Borders(
                    new Border( // Index 0 - The default border.
                        new LeftBorder(),
                        new RightBorder(),
                        new TopBorder(),
                        new BottomBorder(),
                        new DiagonalBorder()),
                    new Border( // Index 1 - Applies a Left, Right, Top, Bottom border to a cell
                        new LeftBorder(
                            new Color {Auto = true}
                            ) {Style = BorderStyleValues.Thin},
                        new RightBorder(
                            new Color {Auto = true}
                            ) {Style = BorderStyleValues.Thin},
                        new TopBorder(
                            new Color {Auto = true}
                            ) {Style = BorderStyleValues.Thin},
                        new BottomBorder(
                            new Color {Auto = true}
                            ) {Style = BorderStyleValues.Thin},
                        new DiagonalBorder())
                    ),
                new CellFormats(
                    new CellFormat {FontId = 0, FillId = 0, BorderId = 0},
                    // Index 0 - The default cell style.  If a cell does not have a style index applied it will use this style combination instead
                    new CellFormat {FontId = 1, FillId = 0, BorderId = 0, ApplyFont = true}, // Index 1 - Bold
                    new CellFormat {FontId = 2, FillId = 0, BorderId = 0, ApplyFont = true}, // Index 2 - Italic
                    new CellFormat {FontId = 3, FillId = 0, BorderId = 0, ApplyFont = true}, // Index 3 - Times Roman
                    new CellFormat {FontId = 0, FillId = 2, BorderId = 0, ApplyFill = true}, // Index 4 - Yellow Fill
                    new CellFormat( // Index 5 - Alignment
                        new Alignment
                        {
                            Horizontal = HorizontalAlignmentValues.Center,
                            Vertical = VerticalAlignmentValues.Center
                        }
                        ) {FontId = 0, FillId = 0, BorderId = 0, ApplyAlignment = true},
                    new CellFormat {FontId = 0, FillId = 0, BorderId = 1, ApplyBorder = true}, // Index 6 - Border
                    new CellFormat {NumberFormatId = 165, ApplyNumberFormat = true ,FontId = 0, FillId = 0, BorderId = 0} //Date format
                    )
                    
                ); // return
        }
    }
}