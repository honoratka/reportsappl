﻿using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReportsApp.Models
{
    public class InfotableModelHelper
    {
      
        public PdfPTable Infotable { get; set; }
       
        public string Id { get; set; }
    
        public DateTime DateTime { get; set; }

    }
}
