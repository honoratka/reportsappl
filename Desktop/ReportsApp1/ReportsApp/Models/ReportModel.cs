﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//Validation
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Runtime.Serialization;
using System.Globalization;
using System.Net.Http;

namespace ReportsApp.Models
{
    public class Rsponse
    {
        public bool result { get; set; }
        public string message { get; set; }
        public string id { get; set; }
    }


    public class entry
    { 
        public DateTime DateTime { get; set; }      
    }


    public class ReportModel
    {
        public string directUrl { get; set; }

        public string ConfigColumns { get; set; }

        public List<ReportComment> comments = new List<ReportComment>();

        public List<entry> entries = new List<entry>();

        public List<CorrectiveAction>  CorrectiveActions = new List<CorrectiveAction>();
      
        public List<string> Handlers = new List<string>();       
        
        public ReportStatus Status { get; set; }

        public DateTime DateTimeCreated { get; set; }

        [Required(ErrorMessage = "Id is required")]
        [Range(0, int.MaxValue, ErrorMessage = "Not valid integer Number")]
        public string Id { get; set; }

        [Required(ErrorMessage = "Title is required")]
        public string Title { get; set; }


        public ReportModel()
            {
                Descriptions = new List<ReportDescription>();
                CorrectiveActions = new List<CorrectiveAction>();
                //Quantities = new List<Quantity>();
                ParentDimensionSequence = new List<ParentDimension>();
                UserAccesses = new List<ReportAccess>();
                Files = new List<ReportFile>();
                Units = new List<ReportUnit>();
                Tags = new List<string>();
            }

            
            public string SystemId { get; set; }

            /// <summary>
            /// Numeric, unique value for Location ID
            /// </summary>
           
            public int NumericDimensionId { get; set; }

            /// <summary>
            /// Numeric, unique value for Category ID
            /// </summary>
            
            public int NumericCategoryId { get; set; }

            /// <summary>
            /// This is the BSON representation for ID
            /// </summary>
           
            public string DimensionId { get; set; }

            
            public string CategoryId { get; set; }

           
            public string CategoryCode { get; set; }

           
            public string SecurityLevel { get; set; }

            /// <summary>
            /// Capturing the title when reported, because it may change
            /// </summary>
           
            public string CategoryTitle { get; set; }

           
            public DateTime CreatedDateTime { get; set; }

           
            public string CreatedByUserId { get; set; }

          
            public string CreatedByName { get; set; }
        
            public string DimensionUniqueCode { get; set; }
        
            public List<ReportDescription> Descriptions { get; set; }
        
           // public List<CorrectiveAction> CorrectiveActions { get; set; }

           
           // public PotensialImpact PotensialImpact { get; set; }

          
            public string ImpactDescription { get; set; }
        
            public string PotensialImpactQuantityUnit { get; set; }
        
            public double PotensialImpactQuantityPower { get; set; }

         
            //public List<Quantity> Quantities { get; set; }

            /// <summary>
            /// This sEquence if IDs resembles the hierarchy of parents for the dimension
            /// </summary>
           
            public List<ParentDimension> ParentDimensionSequence { get; set; }
        
            public DateTime StartDate { get; set; }

   
            public DateTime EndDate { get; set; }
        
            public List<ReportAccess> UserAccesses { get; set; }


            public List<ReportFile> Files { get; set; }
        
            public List<ReportUnit> Units { get; set; }

        
            //public ReportStatus Status { get; set; }
        
            public List<string> Tags { get; set; }

            
            public int NumericReportId { get; set; }
        
            public string ParentDimensionSequenceString { get; set; }

            
            public string DimensionName { get; set; }
        
            public string NonConformityLevel { get; set; }

            /// <summary>
            /// If there report has been cloned to another report, the ID of new report is placed in this field.
            /// If there is value in this field, the report will not be shown in statistics. 
            /// </summary>
           
            public string CloneId { get; set; }

           
            public string GeoData { get; set; }

            
            public bool ReadTimeOut { get; set; }

          
            public bool HandlingTimeOut { get; set; }

           
            public DateTime LastExpiryProcessDate { get; set; }

            
            public DateTime HandlingExpiryNotificationDate { get; set; }

            
            public bool IsPartOfTendency { get; set; }
        }


    public class ParentDimension
    {
        public string DimensionId { get; set; }

        public int NumericId { get; set; }

    }

    public class ReportDescription
        {
           
            public DescriptionType Type { get; set; }

            
            public string Description { get; set; }
        }

        public enum DescriptionType
        {
            General = 0,
            Situation = 1,
            Reason = 2,
            Consequence = 3,
            ActionsTaken = 4,
            Solution = 5
        }

       public enum ReportStatus
        {
            Created = 0,
            Read = 10,
            ReadByHandler = 20,
            HandlingStarted = 30,
            OnHold = 40,
            Forwarded = 50,
            CorrectiveActionStarted = 60,
            CorrectiveActionCompletedPendingResult = 70,
            Completed = 90
        }


    public class ReportUnit
        {
            
            public string Name { get; set; }
            
            public string Unit { get; set; }
          
            public float Power { get; set; }

        }


      
        public class ReportAccess
        {
            
            public string UserId { get; set; }

            
            public int AccessLevel { get; set; }
        
            public bool IsHandler { get; set; }
          
            public bool IsObserver { get; set; }

           
            public int Notification { get; set; }
           
            public bool Locked { get; internal set; }
            
            public bool CanDelete { get; set; }
      
            public bool CanEdit { get; set; }
        }

        
        public class ReportFile
        {
           
            public string FileId { get; set; }

            public string Bytes { get; set; }
            public string Filename { get; set; }
        
            public long Size { get; set; }

            public string ContentType { get; set; }
        }

    public class CorrectiveAction 
    {
             
        public string SystemId { get; set; }      
      
        public string ReportId { get; set; }
       
        public DateTime CreateDateTime { get; set; }
       
        public DateTime ExpectedImplementationDate { get; set; }
       
        public DateTime ResultDate { get; set; }
       
        public string ResultComment { get; set; }
       
        public int ImplementationDegree { get; set; }       
      
        public string ImplementationUser { get; set; }
       
        public bool PrimaryAction { get; set; }
       
        public List<string> AssignedTo { get; set; }

        public CorrectiveAction()
        {
            AssignedTo = new List<string>();
        }
       
        public string Description { get; set; }
    }

    public class ReportComment
    {       
    
        public string SystemId { get; set; }
        
        public string CommentText { get; set; }        
       
        public string ReportId { get; set; }
        
        public int NumericReportId { get; set; }
        
        public DateTime DateTimeCreated { get; set; }
        
        public string CreatedBy { get; set; }        
       
        public Guid Guid { get; set; }
        
        public int ReportStatus { get; set; }


    }

}

