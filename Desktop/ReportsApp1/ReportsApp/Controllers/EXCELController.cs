﻿using System;
using System.Collections.Generic;
using System.Linq;
using iTextSharp.text;
using System.IO;
using iTextSharp.text.pdf;
using System.Threading.Tasks;
using ReportsApp.Models;
using System.Web.Http;
using System.Diagnostics;
using System.Drawing;
using System.Resources;
using System.Globalization;
using ImageProcessor;
using Image = System.Drawing.Image;
using ImageProcessor.Imaging;
using System.Drawing.Imaging;
using System.Configuration;
using ReportsApp.Reporting;
using Itenso.TimePeriod;

namespace iTextSharp.simple.Part1
{
    public class EXCELController : ApiController
    {
        public ResourceManager rm = null;

        //constructor - should handle injection - Unity
        public EXCELController()
        {
            rm = new ResourceManager("ReportsApp.Resources.LanguageResxFiles.pagedialogs", System.Reflection.Assembly.GetExecutingAssembly());
        }

        // [System.Web.Http.HttpPost]
        //[Authorize]
        //[AcceptVerbs(HttpVerbs.Post)]
        public async Task<IHttpActionResult> Post(ReportListModel models)
        {
            //simple checking ApiKey & ApiPassword
            string ApiKey = System.Configuration.ConfigurationManager.AppSettings["ApiKey"];
            string ApiPassword = System.Configuration.ConfigurationManager.AppSettings["ApiPassword"];

            if (models.ApiKey != ApiKey) return Json(new Rsponse { message = "Wrong ApiKey", result = false });
            if (models.ApiPassword != ApiPassword) return Json(new Rsponse { message = "Wrong ApiPassword", result = false });

            //Correction: The incoming model should hold preferences, logo, etc. Use ReportListModel instead. I wrote this class for you...
            //There should be two different end points as well. 
            //api/Report & api/ReportList        

            var modelState = ModelState.Values;

            foreach (var m in models.Reports)
            {
                //Correction: eh... when about m here. You are iterating but not testing m.. Makes no sense this loop TODO
                if (!ModelState.IsValid)
                {
                    return Json(new { result = false, message = ModelState.Values.SelectMany(n => n.Errors) });
                }
            }
            //Json response
            Rsponse rsponse = new Rsponse();


            CultureInfo.DefaultThreadCurrentCulture = new CultureInfo(models.Culture);

            var svc = new SpreadSheetService();
            svc.CreateSpreadsheet();
            var oneSheet = svc.AddSheet(ReportsApp.Helpers.Helpers.StringResXLanguageConversion(rm, "SheetNameReportsLabel", new CultureInfo(models.Culture)));


            // null checker
            
            var excelColumnEmptyNumberList = new List<ExcelColumnEmptyNumber>();
            int colnumber = 0;

            foreach (var i in models.Reports)
            {
                var dateDiff = new DateDiff(i.StartDate, i.EndDate);
                var listofstring = new List<string>() { ReportsApp.Helpers.Helpers.StringResXLanguageConversion(rm, "ViewLinkText", new CultureInfo(models.Culture)),
                      i.ParentDimensionSequenceString + "//" + i.DimensionName,
                    i.NumericReportId.ToString(),
                    i.CategoryCode,
                    i.CategoryTitle,
                    i.CreatedByName,
                    i.CreatedDateTime.ToLocalTime().ToString("O"),
                    i.StartDate.ToLocalTime().ToString("O"),
                    i.EndDate.ToLocalTime().ToString("O"),
                    Math.Round(dateDiff.Difference.TotalMinutes, 2).ToString(CultureInfo.InvariantCulture),
                    i.Status.ToString(),
                    ((int)i.Status).ToString(),
                    i.Tags != null ? string.Join(", ", i.Tags) : "",
                    i.NonConformityLevel ?? "",
                    i.Units != null && i.Units.Any() && i.Units.First() != null ? i.Units.First().Name : "",
                    i.Units != null && i.Units.Any() && i.Units.First() != null ? i.Units.First().Power.ToString(CultureInfo.InvariantCulture) : "",
                    i.Units != null && i.Units.Any() && i.Units.First() != null ? i.Units.First().Unit : "",

                    i.Units != null && i.Units.Count > 1 && i.Units[1] != null ? i.Units[1].Name : "",
                    i.Units != null && i.Units.Count > 1 && i.Units[1] != null ? i.Units[1].Power.ToString(CultureInfo.InvariantCulture) : "",
                    i.Units != null && i.Units.Count > 1 && i.Units[1] != null ? i.Units[1].Unit : "",

                    i.Units != null && i.Units.Count > 2 && i.Units[2] != null ? i.Units[2].Name : "",
                    i.Units != null && i.Units.Count > 2 && i.Units[2] != null ? i.Units[2].Power.ToString(CultureInfo.InvariantCulture) : "",
                    i.Units != null && i.Units.Count > 2 && i.Units[2] != null ? i.Units[2].Unit : "",

                    i.Descriptions.Any(x => x.Type == DescriptionType.General) ? i.Descriptions.First(x => x.Type == DescriptionType.General).Description : "",
                    i.Descriptions.Any(x => x.Type == DescriptionType.Situation) ? i.Descriptions.First(x => x.Type == DescriptionType.Situation).Description : "",
                    i.Descriptions.Any(x => x.Type == DescriptionType.Consequence) ? i.Descriptions.First(x => x.Type == DescriptionType.Consequence).Description : "",
                    i.Descriptions.Any(x => x.Type == DescriptionType.ActionsTaken) ? i.Descriptions.First(x => x.Type == DescriptionType.ActionsTaken).Description : "",
                    i.Descriptions.Any(x => x.Type == DescriptionType.Reason) ? i.Descriptions.First(x => x.Type == DescriptionType.Reason).Description : "",
                    i.Descriptions.Any(x => x.Type == DescriptionType.Solution) ? i.Descriptions.First(x => x.Type == DescriptionType.Solution).Description : "",

                    i.Tags != null && i.Tags.Any() && i.Tags.Count > 0 ? i.Tags[0] : "",
                    i.Tags != null && i.Tags.Any() && i.Tags.Count > 1 ? i.Tags[1] : "",
                    i.Tags != null && i.Tags.Any() && i.Tags.Count > 2 ? i.Tags[2] : "",
                    i.Tags != null && i.Tags.Any() && i.Tags.Count > 3 ? i.Tags[3] : "",
                    i.Tags != null && i.Tags.Any() && i.Tags.Count > 4 ? i.Tags[4] : "", };

                colnumber = 0;
                foreach (string li in listofstring) {

                    excelColumnEmptyNumberList.Add( ReportsApp.Helpers.Helpers.NullChceker(colnumber, li ));
                    colnumber++;
                } 
            }

            var columnLabelsFull = new List<string> {  ReportsApp.Helpers.Helpers.StringResXLanguageConversion(rm, "DirectLinkToReportLabel", new CultureInfo(models.Culture)),
                ReportsApp.Helpers.Helpers.StringResXLanguageConversion(rm, "DimensionGeneric", new CultureInfo(models.Culture)),
                ReportsApp.Helpers.Helpers.StringResXLanguageConversion(rm, "ReportIdNumeric", new CultureInfo(models.Culture)),
                ReportsApp.Helpers.Helpers.StringResXLanguageConversion(rm, "CategoryCode", new CultureInfo(models.Culture)),
                ReportsApp.Helpers.Helpers.StringResXLanguageConversion(rm, "ReportCategoryLabel", new CultureInfo(models.Culture)),
                ReportsApp.Helpers.Helpers.StringResXLanguageConversion(rm, "CreatedByLabel", new CultureInfo(models.Culture)),
                ReportsApp.Helpers.Helpers.StringResXLanguageConversion(rm, "CreatedDateTimeLabel", new CultureInfo(models.Culture)),
                ReportsApp.Helpers.Helpers.StringResXLanguageConversion(rm, "OccurrenceDateLabel", new CultureInfo(models.Culture)),
                ReportsApp.Helpers.Helpers.StringResXLanguageConversion(rm, "EndDateLabel", new CultureInfo(models.Culture)),
                ReportsApp.Helpers.Helpers.StringResXLanguageConversion(rm, "DurationInMinutes", new CultureInfo(models.Culture)),
                ReportsApp.Helpers.Helpers.StringResXLanguageConversion(rm, "StatusDescriptionLabel", new CultureInfo(models.Culture)),
                ReportsApp.Helpers.Helpers.StringResXLanguageConversion(rm, "StatusCodeLabel", new CultureInfo(models.Culture)),
                ReportsApp.Helpers.Helpers.StringResXLanguageConversion(rm, "TagsGeneric", new CultureInfo(models.Culture)),
                ReportsApp.Helpers.Helpers.StringResXLanguageConversion(rm, "NonConformityLevel", new CultureInfo(models.Culture)),
                ReportsApp.Helpers.Helpers.StringResXLanguageConversion(rm, "CategoryUnit", new CultureInfo(models.Culture)),
                ReportsApp.Helpers.Helpers.StringResXLanguageConversion(rm, "UnitPower", new CultureInfo(models.Culture)),
                ReportsApp.Helpers.Helpers.StringResXLanguageConversion(rm, "UnitGeneric", new CultureInfo(models.Culture)),
                ReportsApp.Helpers.Helpers.StringResXLanguageConversion(rm, "CategoryUnit", new CultureInfo(models.Culture)),
                ReportsApp.Helpers.Helpers.StringResXLanguageConversion(rm, "UnitPower", new CultureInfo(models.Culture)),
                ReportsApp.Helpers.Helpers.StringResXLanguageConversion(rm, "UnitGeneric", new CultureInfo(models.Culture)),
                ReportsApp.Helpers.Helpers.StringResXLanguageConversion(rm, "CategoryUnit", new CultureInfo(models.Culture)),
                ReportsApp.Helpers.Helpers.StringResXLanguageConversion(rm, "UnitPower", new CultureInfo(models.Culture)),
                ReportsApp.Helpers.Helpers.StringResXLanguageConversion(rm, "UnitGeneric", new CultureInfo(models.Culture)),
                ReportsApp.Helpers.Helpers.StringResXLanguageConversion(rm, "DescriptionLabelGeneric", new CultureInfo(models.Culture)),
                ReportsApp.Helpers.Helpers.StringResXLanguageConversion(rm, "SituationDescriptionLabel", new CultureInfo(models.Culture)),
                ReportsApp.Helpers.Helpers.StringResXLanguageConversion(rm, "ConsequenceDescriptionLabel", new CultureInfo(models.Culture)),
                ReportsApp.Helpers.Helpers.StringResXLanguageConversion(rm, "ActionsTakenDescriptionLabel", new CultureInfo(models.Culture)),
                ReportsApp.Helpers.Helpers.StringResXLanguageConversion(rm, "ReasonDescriptionLabel", new CultureInfo(models.Culture)),
                ReportsApp.Helpers.Helpers.StringResXLanguageConversion(rm, "SolutionDescriptionLabel", new CultureInfo(models.Culture)),
                ReportsApp.Helpers.Helpers.StringResXLanguageConversion(rm, "TagGeneric", new CultureInfo(models.Culture)),
                ReportsApp.Helpers.Helpers.StringResXLanguageConversion(rm, "TagGeneric", new CultureInfo(models.Culture)),
                ReportsApp.Helpers.Helpers.StringResXLanguageConversion(rm, "TagGeneric", new CultureInfo(models.Culture)),
                ReportsApp.Helpers.Helpers.StringResXLanguageConversion(rm, "TagGeneric", new CultureInfo(models.Culture)),
                ReportsApp.Helpers.Helpers.StringResXLanguageConversion(rm, "TagGeneric", new CultureInfo(models.Culture)),};


            var columnLabelsCutted = new List<string>();

            colnumber = 0;
            foreach (var label in columnLabelsFull)
            {
                if (excelColumnEmptyNumberList[colnumber].IsColumnEmpty == false ) columnLabelsCutted.Add(columnLabelsFull[colnumber]);
                colnumber++;
            }

            svc.CreateColumnWidth(oneSheet, 4, 6, 20);
            svc.AddHeader(oneSheet, columnLabelsCutted);


            foreach (var i in models.Reports)
            {
                colnumber = 0;
                var directUrl = i.Id;
                var dateDiff = new DateDiff(i.StartDate, i.EndDate);
                var rowsDataFull = new List<string>
                {
                       ReportsApp.Helpers.Helpers.StringResXLanguageConversion(rm, "ViewLinkText", new CultureInfo(models.Culture)),
                      i.ParentDimensionSequenceString + "//" + i.DimensionName,
                    i.NumericReportId.ToString(),
                    i.CategoryCode,
                    i.CategoryTitle,
                    i.CreatedByName,
                    i.CreatedDateTime.ToLocalTime().ToString("O"),
                    i.StartDate.ToLocalTime().ToString("O"),
                    i.EndDate.ToLocalTime().ToString("O"),
                    Math.Round(dateDiff.Difference.TotalMinutes,2).ToString(CultureInfo.InvariantCulture),
                    i.Status.ToString(),
                    ((int)i.Status).ToString(),
                    i.Tags!=null ? string.Join(", ",i.Tags): "",
                    i.NonConformityLevel ?? "",
                    i.Units!=null && i.Units.Any() && i.Units.First()!=null ? i.Units.First().Name : "",
                    i.Units!=null && i.Units.Any() && i.Units.First()!=null ? i.Units.First().Power.ToString(CultureInfo.InvariantCulture): "",
                    i.Units!=null && i.Units.Any() && i.Units.First()!=null ? i.Units.First().Unit: "",

                    i.Units!=null && i.Units.Count>1 && i.Units[1]!=null ? i.Units[1].Name : "",
                    i.Units!=null && i.Units.Count>1 && i.Units[1]!=null ? i.Units[1].Power.ToString(CultureInfo.InvariantCulture): "",
                    i.Units!=null && i.Units.Count>1 && i.Units[1]!=null ? i.Units[1].Unit: "",

                    i.Units!=null && i.Units.Count>2 && i.Units[2]!=null ? i.Units[2].Name : "",
                    i.Units!=null && i.Units.Count>2 && i.Units[2]!=null ? i.Units[2].Power.ToString(CultureInfo.InvariantCulture): "",
                    i.Units!=null && i.Units.Count>2 && i.Units[2]!=null ? i.Units[2].Unit: "",

                    i.Descriptions.Any(x=> x.Type == DescriptionType.General) ? i.Descriptions.First(x=> x.Type==DescriptionType.General).Description : "",
                    i.Descriptions.Any(x=> x.Type == DescriptionType.Situation) ? i.Descriptions.First(x=> x.Type==DescriptionType.Situation).Description : "",
                    i.Descriptions.Any(x=> x.Type == DescriptionType.Consequence) ? i.Descriptions.First(x=> x.Type==DescriptionType.Consequence).Description : "",
                    i.Descriptions.Any(x=> x.Type == DescriptionType.ActionsTaken) ? i.Descriptions.First(x=> x.Type==DescriptionType.ActionsTaken).Description : "",
                    i.Descriptions.Any(x=> x.Type == DescriptionType.Reason) ? i.Descriptions.First(x=> x.Type==DescriptionType.Reason).Description : "",
                    i.Descriptions.Any(x=> x.Type == DescriptionType.Solution) ? i.Descriptions.First(x=> x.Type==DescriptionType.Solution).Description : "",

                    i.Tags!=null && i.Tags.Any() && i.Tags.Count > 0 ? i.Tags[0] : "",
                    i.Tags!=null && i.Tags.Any() && i.Tags.Count > 1 ? i.Tags[1] : "",
                    i.Tags!=null && i.Tags.Any() && i.Tags.Count > 2 ? i.Tags[2] : "",
                    i.Tags!=null && i.Tags.Any() && i.Tags.Count > 3 ? i.Tags[3] : "",
                    i.Tags!=null && i.Tags.Any() && i.Tags.Count > 4 ? i.Tags[4] : ""

                };

                var dataCutted = new List<string>();
                foreach (var row in rowsDataFull)
                {
                    if (excelColumnEmptyNumberList[colnumber].IsColumnEmpty == false) dataCutted.Add(rowsDataFull[colnumber]);
                    colnumber++;
                }




                svc.AddRow(oneSheet, dataCutted, i.directUrl);             
            }
            svc.SaveChanges(oneSheet);
                svc.CloseSpreadsheet();

                svc.SpreadsheetStream.Position = 0;
                //  Session["sheet"] = svc.SpreadsheetStream;

                //convert memorystream to byte array         
                svc.SpreadsheetStream.CopyTo(svc.SpreadsheetStream);
                byte[] msAsByte = svc.SpreadsheetStream.ToArray();
                svc.SpreadsheetStream.Close();

                //  convert byte array to base64
                var msAsBase64Byte = Convert.ToBase64String(msAsByte);

                rsponse = new Rsponse
                {
                    result = true,
                    message = msAsBase64Byte,
                    id = "4",
                };
            
        return Json(rsponse);            
        }


        private async Task<byte[]> RenderLogolocal(string LogoBase64String)
        {
            //wrapping it all in a task. If not the async statement has no sense
            return await Task.Run(() =>
            {
                var files = LogoBase64String;
                if (files.Any())
                {
                    using (var stream = new MemoryStream())
                    {
                        var bytes = ReportsApp.Helpers.Helpers.imageToByteArray((Image)ReportsApp.Resources.Images.Images.logo_oa);
                        stream.Read(bytes, 0, (int)stream.Length);

                        using (var filestream = new MemoryStream(bytes))
                        {
                            using (var outStream = new MemoryStream())
                            {
                                using (var imageFactory = new ImageFactory(true))
                                {

                                    var fact = imageFactory.Load(filestream);
                                    fact.Resize(new ResizeLayer(new Size(320, 320), ResizeMode.Max));
                                    fact.Save(outStream);
                                    outStream.Position = 0;
                                    return outStream.ToArray();
                                }
                            }
                        }
                    }
                }

                var oBitmap = new Bitmap(180, 60);
                var oGraphic = Graphics.FromImage(oBitmap);
                var oColor = Color.Black;
                var oBrush = new SolidBrush(oColor);
                oGraphic.FillRectangle(oBrush, 0, 0, 180, 60);
                var oBrushWrite = new SolidBrush(Color.White);
                var oFont = new System.Drawing.Font("Arial", 13);
                var oPoint = new PointF(8f, 8f);
                oGraphic.DrawString("No logo defined", oFont, oBrushWrite, oPoint);

                // Response.ContentType = "image/png";

                //better to use stream like this. It is done above, so why not here?
                using (var str = new MemoryStream())
                {
                    oBitmap.Save(str, ImageFormat.Png);
                    return str.GetBuffer();
                }
            });
        }

        private string GetStatusDescription(ReportStatus status, CultureInfo cultureInfo)
        {
            switch (status)
            {
                case ReportStatus.Created:
                    return ReportsApp.Helpers.Helpers.StringResXLanguageConversion(rm, "StatusCreated", cultureInfo);
                case ReportStatus.Read:
                    return ReportsApp.Helpers.Helpers.StringResXLanguageConversion(rm, "StatusCreated", cultureInfo);
                case ReportStatus.ReadByHandler:
                    return ReportsApp.Helpers.Helpers.StringResXLanguageConversion(rm, "StatusReadByHandler", cultureInfo);
                case ReportStatus.HandlingStarted:
                    return ReportsApp.Helpers.Helpers.StringResXLanguageConversion(rm, "StatusHandlingStarted", cultureInfo);
                case ReportStatus.OnHold:
                    return ReportsApp.Helpers.Helpers.StringResXLanguageConversion(rm, "StatusCreated", cultureInfo);
                case ReportStatus.Forwarded:
                    return ReportsApp.Helpers.Helpers.StringResXLanguageConversion(rm, "StatusForwarded", cultureInfo);
                case ReportStatus.CorrectiveActionStarted:
                    return ReportsApp.Helpers.Helpers.StringResXLanguageConversion(rm, "StatusCorrectiveActionStarted", cultureInfo);
                case ReportStatus.CorrectiveActionCompletedPendingResult:
                    return ReportsApp.Helpers.Helpers.StringResXLanguageConversion(rm, "StatusCorrectiveActionCompletedPendingResult", cultureInfo);
                case ReportStatus.Completed:
                    return ReportsApp.Helpers.Helpers.StringResXLanguageConversion(rm, "StatusCompleted", cultureInfo);
                default:
                    throw new ArgumentOutOfRangeException(nameof(status), status, null);
            }
        }

        private string GetDescriptionTypeLabel(DescriptionType type, CultureInfo cultureInfo)
        {
            switch (type)
            {
                case DescriptionType.General:
                    return ReportsApp.Helpers.Helpers.StringResXLanguageConversion(rm, "DescriptionLabelGeneric", cultureInfo);
                case DescriptionType.Situation:
                    return ReportsApp.Helpers.Helpers.StringResXLanguageConversion(rm, "SituationDescriptionLabel", cultureInfo);
                case DescriptionType.Reason:
                    return ReportsApp.Helpers.Helpers.StringResXLanguageConversion(rm, "ReasonDescriptionLabel", cultureInfo);
                case DescriptionType.Consequence:
                    return ReportsApp.Helpers.Helpers.StringResXLanguageConversion(rm, "ConsequenceDescriptionLabel", cultureInfo);
                case DescriptionType.ActionsTaken:
                    return ReportsApp.Helpers.Helpers.StringResXLanguageConversion(rm, "ActionsTakenDescriptionLabel", cultureInfo);
                case DescriptionType.Solution:
                    return ReportsApp.Helpers.Helpers.StringResXLanguageConversion(rm, "SolutionDescriptionLabel", cultureInfo);
                default:
                    throw new ArgumentOutOfRangeException(nameof(type), type, null);
            }
        }
    }
}